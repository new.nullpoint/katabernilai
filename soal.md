Anda mewawancarai seorang atasan, banyak sekali hal yang anda tanyakan, dan sang atasan pun berbaik hati menjelaskan secara sangat detail apa yang anda tanyakan. Anda selalu mencatat setiap kata yang diucapkan oleh sang atasan. Setelah dilihat2 kembali catatan tersebut, anda memiliki ide untuk membuat suatu tantangan, yaitu menemukan salah satu kata yang bernilai paling tinggi diantara kalimat yang anda catat.

Nilai dari kata akan dinilai dengan jumlah dari urutan alphabeth yang berada dalam kata tersebut, sebagai contoh, a memiliki nilai 1, b memiliki nilai 2, dan seterusnya.

Semisalnya, ada kalimat "aa bb cc", maka kata yang bernilai paling tinggi adalah "cc" yang memiliki nilai 6(3+3), sedangkan untuk "aa", bernilai 2, "bb" bernilai 4.

Kamu akan mencari kata yang mempunyai nilai tertinggi antara kalimat tersebut.


**Input Format**
**T** yang akan menyatakan jumlah kasus uji T baris berikutnya berisi kalimat.

**Constraints**
3<=T<=25, 1<=panjang kalimat<=150


**Output Format**
**T** buah baris yang berisikan kata terpanjang dari kalimat

**Sample Input 0**
```csharp
3
saya butuh taksi untuk berangkat kehotel
saya belajar program lima tahun yang lalu
bahasa yang saya pelajari adalah cplusplus dan python
```


**Sample Output 0**
```csharp
untuk
program
cplusplus
```
